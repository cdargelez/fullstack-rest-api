import api from "./utils/api"

const greetings = async () => {
    // const message: string = "Hello web CPROM"
    const message = await api.get('/');
    console.log('Axios response ->', message)
    console.log('Axios response ->', message.data)
}
greetings();

var btnTogeleIsActiv: boolean = false
var wrapperBTN = document.getElementById('wrapperBTN')
var btnTogele = document.getElementById('btnTogele')

btnTogele.addEventListener('click', ()=>{
    document.body.classList.toggle('dack')
    if(!btnTogeleIsActiv){
        btnTogeleIsActiv = true
        btnTogele.classList.add("move_left")
        btnTogele.classList.remove("move_right")
        wrapperBTN.style.backgroundColor = "white"
    }else if(btnTogeleIsActiv){
        btnTogeleIsActiv = false
        btnTogele.classList.add("move_right")
        btnTogele.classList.remove("move_left")
        wrapperBTN.style.backgroundColor = "black"
    }
})